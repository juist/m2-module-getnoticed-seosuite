<?php

namespace GetNoticed\SeoSuite\Model\Script;

/**
 * Class DataProvider
 *
 * @package GetNoticed\SeoSuite\Model\Script
 */
class DataProvider
    extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    /**
     * @var \GetNoticed\SeoSuite\Model\ResourceModel\Script\CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var \Magento\Backend\Model\Session
     */
    protected $session;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Json $jsonSerializer
     */
    protected $jsonSerializer;


    /**
     * DataProvider constructor.
     *
     * @param \Magento\Framework\Serialize\Serializer\Json                      $jsonSerializer
     * @param \Magento\Backend\Model\Session                                    $session
     * @param \GetNoticed\SeoSuite\Model\ResourceModel\Script\CollectionFactory $collectionFactory
     * @param string                                                            $name
     * @param string                                                            $primaryFieldName
     * @param string                                                            $requestFieldName
     * @param array                                                             $meta
     * @param array                                                             $data
     */
    public function __construct(
        \Magento\Framework\Serialize\Serializer\Json $jsonSerializer,
        \Magento\Backend\Model\Session $session,
        \GetNoticed\SeoSuite\Model\ResourceModel\Script\CollectionFactory $collectionFactory,
        $name,
        $primaryFieldName,
        $requestFieldName,
        array $meta = [],
        array $data = []
    ) {
        $this->jsonSerializer = $jsonSerializer;
        $this->session = $session;
        $this->collection = $collectionFactory->create();

        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);

    }

    /**
     * @return array
     */
    public function getData()
    {
        if ($this->loadedData === null) {
            $this->loadedData = [];

            foreach ($this->getCollection()->getItems() as $item) {
                /** @var \GetNoticed\SeoSuite\Model\Script $item */
                $this->loadedData[$item->getId()] = [
                    'script' => array_merge(
                        $item->getData(),
                        [
                            'id_field_name' => $item->getIdFieldName(),
                            'stores'        => []
                        ]
                    )
                ];

                foreach ($item->getStores() as $store) {
                    $this->loadedData[$item->getId()]['script']['stores'][] = $store->getId();
                }
            }
        }

        $formData = $this->session->getScriptFormData();
        if (!empty($formData)) {
            $entityId = isset($data['script']['script_id']) ? $formData['script']['script_id'] : null;
            $this->loadedData[$entityId] = $formData;
            $this->session->unsScriptFormData();
        }

        return $this->loadedData;
    }

}