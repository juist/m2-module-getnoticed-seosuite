<?php

namespace GetNoticed\SeoSuite\Model\ResourceModel;

use GetNoticed\SeoSuite;
use Magento\Store;
use Magento\Framework;

/**
 * Class Script
 *
 * @package GetNoticed\SeoSuite\Model\ResourceModel
 */
class Script
    extends Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Table name
     */
    const TABLE_NAME = 'getnoticed_seo_script';

    /**
     * Store Table
     */
    const TABLE_NAME_STORE = 'getnoticed_seo_script_store';

    /**
     * @var SeoSuite\Model\ResourceModel\ScriptStore\CollectionFactory
     */
    protected $scriptStoreCollectionFactory;

    /**
     * @var SeoSuite\Model\ScriptStoreFactory
     */
    protected $scriptStoreFactory;

    /**
     * @var SeoSuite\Model\ResourceModel\ScriptStore
     */
    protected $scriptStoreResource;

    /**
     * @var Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Script constructor.
     *
     * @param \GetNoticed\SeoSuite\Model\ResourceModel\ScriptStore\CollectionFactory $scriptStoreCollectionFactory
     * @param \GetNoticed\SeoSuite\Model\ScriptStoreFactory                          $scriptStoreFactory
     * @param \GetNoticed\SeoSuite\Model\ResourceModel\ScriptStore                   $scriptStoreResource
     * @param \Magento\Store\Model\StoreManagerInterface                             $storeManager
     * @param \Magento\Framework\Model\ResourceModel\Db\Context                      $context
     * @param null                                                                   $connectionName
     */
    public function __construct(
        SeoSuite\Model\ResourceModel\ScriptStore\CollectionFactory $scriptStoreCollectionFactory,
        SeoSuite\Model\ScriptStoreFactory $scriptStoreFactory,
        SeoSuite\Model\ResourceModel\ScriptStore $scriptStoreResource,
        Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    ) {
        $this->scriptStoreCollectionFactory = $scriptStoreCollectionFactory;
        $this->scriptStoreFactory = $scriptStoreFactory;
        $this->scriptStoreResource = $scriptStoreResource;
        $this->storeManager = $storeManager;

        parent::__construct($context, $connectionName);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            self::TABLE_NAME,
            SeoSuite\Model\ResourceModel\Script\Collection::ID_FIELD_NAME
        );
    }

    /**
     * @param \GetNoticed\SeoSuite\Api\Data\ScriptInterface|\GetNoticed\SeoSuite\Model\Script $script
     */
    public function loadStores(SeoSuite\Api\Data\ScriptInterface $script)
    {
        /** @var \Magento\Store\Api\Data\StoreInterface[] $stores */
        $stores = [];

        /** @var SeoSuite\Model\ResourceModel\ScriptStore\Collection $collection */
        $collection = $this->scriptStoreCollectionFactory->create();
        $collection
            ->addFieldToFilter('script_id', ['eq' => $script->getId()]);

        foreach ($collection as $storeLink) {
            /** @var SeoSuite\Model\ScriptStore $storeLink */
            $stores[] = $this->storeManager->getStore($storeLink->getData('store_id'));
        }

        $script->setStores($stores);

        return $this;
    }

    /**
     * @param \GetNoticed\SeoSuite\Api\Data\ScriptInterface $script
     * @param array                                         $storeIds
     *
     * @return $this
     */
    public function loadStoresByIds(SeoSuite\Api\Data\ScriptInterface $script, array $storeIds)
    {
        /** @var \Magento\Store\Api\Data\StoreInterface[] $stores */
        $stores = [];

        foreach ($storeIds as $storeId) {
            $stores[] = $this->storeManager->getStore($storeId);
        }

        $script->setStores($stores);

        return $this;
    }

    /**
     * @param Framework\Model\AbstractModel|SeoSuite\Api\Data\ScriptInterface $object
     *
     * @return $this|Framework\Model\ResourceModel\Db\AbstractDb
     */
    protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
        if ($object->isObjectNew() === true) {
            $object->setCreatedAt(new \DateTime());
        }
        $object->setUpdatedAt(new \DateTime());

        return parent::_beforeSave($object);
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel|\GetNoticed\SeoSuite\Api\Data\ScriptInterface $object
     *
     * @return $this|\Magento\Framework\Model\ResourceModel\Db\AbstractDb
     */
    protected function _afterSave(\Magento\Framework\Model\AbstractModel $object)
    {
        // Remove current
        /** @var \GetNoticed\SeoSuite\Model\ResourceModel\ScriptStore\Collection $scriptStores */
        $scriptStores = $this->scriptStoreCollectionFactory->create();
        $scriptStores
            ->addFieldToFilter('script_id', ['eq' => $object->getId()]);

        foreach ($scriptStores as $scriptStore) {
            $this->scriptStoreResource->delete($scriptStore);
        }

        // Add new stores. If all stores are selected, add none (automatically treated as all stores)
        if (count($object->getStores()) !== count($this->storeManager->getStores())) {
            foreach ($object->getStores() as $store) {
                /** @var \GetNoticed\SeoSuite\Api\Data\ScriptStoreInterface|\GetNoticed\SeoSuite\Model\ScriptStore $scriptStore */
                $scriptStore = $this->scriptStoreFactory->create();
                $scriptStore
                    ->setScriptId($object->getId())
                    ->setStoreId($store->getId());

                $this->scriptStoreResource->save($scriptStore);
            }
        }

        return parent::_afterSave($object);
    }

}