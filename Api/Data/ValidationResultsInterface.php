<?php

namespace GetNoticed\SeoSuite\Api\Data;

/**
 * Interface ValidationResultsInterface
 *
 * @package GetNoticed\SeoSuite\Api\Data
 */
interface ValidationResultsInterface
{

    const VALID = 'valid';
    const MESSAGES = 'messages';

    /**
     * Check if the provided data is valid.
     *
     * @return bool
     */
    public function isValid();

    /**
     * Set if the provided data is valid.
     *
     * @param bool $isValid
     *
     * @return $this
     */
    public function setIsValid($isValid);

    /**
     * Get error messages as array in case of validation failure, else return empty array.
     *
     * @return string[]
     */
    public function getMessages();

    /**
     * Set error messages as array in case of validation failure.
     *
     * @param string[] $messages
     *
     * @return string[]
     */
    public function setMessages(array $messages);

}