<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use Magento\Framework;
use Magento\Backend;
use Magento\Theme;

/**
 * Class Edit
 *
 * @package GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts
 */
class Edit
    extends AbstractWithEntity
{

    /**
     * Admin Resource
     */
    const ADMIN_RESOURCE = 'GetNoticed_SeoSuite::scripts';

    /**
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        /** @var Backend\Model\View\Result\Page $responsePage */
        $responsePage = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_PAGE);

        try {
            try {
                $script = $this->getScript();
                $responsePage->getConfig()->getTitle()->set(__('Edit script %1', $script->getLabel()));
            } catch (\Exception $e) {
                $script = $this->scriptFactory->create();
                $responsePage->getConfig()->getTitle()->set(__('Create script'));
            }

            return $responsePage;
        } catch (\Exception $e) {
            /** @var Backend\Model\View\Result\Redirect $responseRedirect */
            $responseRedirect = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_REDIRECT);

            $this->messageManager->addErrorMessage($e->getMessage());
            $responseRedirect->setPath('*/*');

            return $responseRedirect;
        }
    }

}