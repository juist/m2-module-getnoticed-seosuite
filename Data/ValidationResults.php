<?php

namespace GetNoticed\SeoSuite\Data;

use GetNoticed\SeoSuite\Api\Data\ValidationResultsInterface;

/**
 * Class ValidationResults
 *
 * @package GetNoticed\SeoSuite\Data
 */
class ValidationResults
    extends \Magento\Framework\Api\AbstractSimpleObject
    implements ValidationResultsInterface
{

    /**
     * {@inheritdoc}
     */
    public function isValid()
    {
        return $this->_get(self::VALID);
    }

    /**
     * {@inheritdoc}
     */
    public function getMessages()
    {
        return $this->_get(self::MESSAGES);
    }

    /**
     * Set if the provided data is valid.
     *
     * @param bool $isValid
     *
     * @return $this
     */
    public function setIsValid($isValid)
    {
        return $this->setData(self::VALID, $isValid);
    }

    /**
     * Set error messages as array in case of validation failure.
     *
     * @param array $messages
     *
     * @return $this
     */
    public function setMessages(array $messages)
    {
        return $this->setData(self::MESSAGES, $messages);
    }
}