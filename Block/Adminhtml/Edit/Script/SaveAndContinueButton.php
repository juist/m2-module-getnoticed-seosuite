<?php

namespace GetNoticed\SeoSuite\Block\Adminhtml\Edit\Script;

class SaveAndContinueButton
    extends AbstractButton
{

    public function getButtonData()
    {
        return [
            'label'          => __(
                $this->getEntityId() === null ? 'Create and Continue Edit' : 'Save and Continue Edit'
            ),
            'class'          => 'save',
            'data_attribute' => [
                'mage-init' => [
                    'button' => ['event' => 'saveAndContinueEdit']
                ]
            ],
            'sort_order'     => 80
        ];
    }

}