<?php

namespace GetNoticed\SeoSuite\Model\ResourceModel\Script;

use GetNoticed\SeoSuite;
use Magento\Store;

/**
 * Class Collection
 *
 * @package GetNoticed\SeoSuite\Model\ResourceModel\Script
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'script_id';

    /**
     * Id Field Name
     */
    const ID_FIELD_NAME = 'script_id';

    /**
     * @var Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @inheritDoc
     */
    public function __construct(
        Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->storeManager = $storeManager;

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $connection, $resource);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(SeoSuite\Model\Script::class, SeoSuite\Model\ResourceModel\Script::class);
    }

    /**
     * @inheritDoc
     */
    protected function _afterLoad()
    {
        foreach ($this->getItems() as $idx => &$item) {
            /** @var \GetNoticed\SeoSuite\Model\Script $item */
            $item->getResource()->loadStores($item);
        }

        return parent::_afterLoad();
    }

    /**
     * @param Store\Model\Store|Store\Api\Data\StoreInterface $store
     *
     * @return $this
     */
    public function addStoreFilter(Store\Model\Store $store)
    {
        $this
            ->getSelect()
            ->joinLeft(
                ['store' => SeoSuite\Model\ResourceModel\Script::TABLE_NAME_STORE],
                'main_table.script_id = store.script_id',
                []
            );
        $this
            ->addFieldToFilter(
                [
                    'store.link_id',
                    'store.store_id'
                ],
                [
                    ['null' => true],
                    ['eq' => $store->getId()]
                ]
            );

        return $this;
    }

}