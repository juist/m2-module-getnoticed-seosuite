<?php

namespace GetNoticed\SeoSuite\Block\Catalog\Category;

use Magento\Catalog;

/**
 * Class View
 *
 * @package GetNoticed\SeoSuite\Block\Catalog\Category
 */
class View
    extends Catalog\Block\Category\View
{

    /**
     * Attribute code: seo title
     */
    const ATTR_CODE_SEO_TITLE = 'seo_title';

    /**
     * Attribute code: seo description
     */
    const ATTR_CODE_SEO_DESCRIPTION = 'seo_description';

    /**
     * @return bool
     */
    public function isShowSeoBlock(): bool
    {
        return strlen($this->getSeoDescription()) > 0;
    }

    /**
     * @return string
     */
    public function getSeoTitle(): string
    {
        if ($this->getCurrentCategory()->getData(self::ATTR_CODE_SEO_TITLE)) {
            return (string)$this->getCurrentCategory()->getData(self::ATTR_CODE_SEO_TITLE);
        }

        return (string)$this->getCurrentCategory()->getName();
    }

    /**
     * @return string
     */
    public function getSeoDescription(): string
    {
        return (string)$this->getCurrentCategory()->getData(self::ATTR_CODE_SEO_DESCRIPTION);
    }

}