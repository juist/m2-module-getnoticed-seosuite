<?php

namespace GetNoticed\SeoSuite\Model\ResourceModel;

use GetNoticed\SeoSuite;

/**
 * Class ScriptStore
 *
 * @package GetNoticed\SeoSuite\Model\ResourceModel
 */
class ScriptStore
    extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Table Name
     */
    const TABLE_NAME = 'getnoticed_seo_script_store';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            self::TABLE_NAME,
            SeoSuite\Model\ResourceModel\ScriptStore\Collection::ID_FIELD_NAME
        );
    }

}