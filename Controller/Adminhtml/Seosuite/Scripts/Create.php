<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use Magento\Framework;
use Magento\Backend;

/**
 * Class Create
 *
 * @package GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts
 */
class Create
    extends AbstractWithEntity
{

    /**
     * Admin Resource
     */
    const ADMIN_RESOURCE = 'GetNoticed_SeoSuite::scripts';

    /**
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        /** @var Backend\Model\View\Result\Forward $response */
        $response = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_FORWARD);
        $response->forward('edit');

        return $response;
    }

}