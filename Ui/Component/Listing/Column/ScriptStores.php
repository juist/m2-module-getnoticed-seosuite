<?php

namespace GetNoticed\SeoSuite\Ui\Component\Listing\Column;

use Magento\Ui;

/**
 * Class ScriptStores
 *
 * @package GetNoticed\SeoSuite\Ui\Component\Listing\Column
 */
class ScriptStores
    extends Ui\Component\Listing\Columns\Column
{

    /**
     * @inheritDoc
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $stores = [];

                foreach ($item['stores'] as $store) {
                    /** @var \Magento\Store\Model\Store $store */
                    $stores[] = sprintf(
                        '%s - %s - %s',
                        $store->getWebsite()->getName(),
                        $store->getGroup()->getName(),
                        $store->getName()
                    );
                }

                $item[$this->getName()] = count($stores) > 0 ? implode('<br/>', $stores) : __('All Stores');
            }
        }

        return parent::prepareDataSource($dataSource);
    }

}