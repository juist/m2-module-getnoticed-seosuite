<?php

namespace GetNoticed\SeoSuite\Block\Adminhtml\Edit\Script;

class ResetButton
    extends AbstractButton
{

    public function getButtonData()
    {
        return [
            'label'      => __('Reset'),
            'class'      => 'reset',
            'on_click'   => 'location.reload()',
            'sort_order' => 30
        ];
    }

}