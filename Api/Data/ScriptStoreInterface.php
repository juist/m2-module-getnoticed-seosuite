<?php

namespace GetNoticed\SeoSuite\Api\Data;

/**
 * Interface ScriptStoreInterface
 *
 * @package GetNoticed\SeoSuite\Api\Data
 */
interface ScriptStoreInterface
{

    /**
     * @param int $storeId
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptStoreInterface
     */
    public function setScriptId(int $storeId): ScriptStoreInterface;

    /**
     * @param int $storeId
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptStoreInterface
     */
    public function setStoreId(int $storeId): ScriptStoreInterface;

}