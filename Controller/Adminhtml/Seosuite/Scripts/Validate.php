<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use Magento\Framework;
use Magento\Backend;

class Validate
    extends AbstractWithEntity
{

    /**
     * @inheritDoc
     */
    public function execute()
    {
        /** @var Framework\Controller\Result\Json $response */
        $response = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_JSON);

        try {
            try {
                $script = $this->getScript();
            } catch (\Exception $e) {
                $script = $this->scriptFactory->create();
            }

            // Script data
            $data = $this->getRequest()->getPostValue();
            $scriptData = $data['script'];
            $scriptStores = $scriptData['stores'];
            unset($scriptData['stores']);

            // Add data to script
            $script->addData($scriptData);
            $script->setStoresByIds($scriptStores);

            // Validate and set response
            $validate = $script->validate();
            $response->setData(
                [
                    'error'    => !$validate->isValid(),
                    'messages' => $validate->getMessages()
                ]
            );
        } catch (\Exception $e) {
            $response->setData(
                [
                    'error'    => true,
                    'messages' => [$e->getMessage()]
                ]
            );
        }

        return $response;
    }

}