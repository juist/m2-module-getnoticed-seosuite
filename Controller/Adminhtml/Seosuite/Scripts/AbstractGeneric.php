<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use Magento\Backend;
use Magento\Framework;
use Magento\Theme;

/**
 * Class AbstractGeneric
 *
 * @package GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts
 * @method Framework\App\Request\Http getRequest()
 */
abstract class AbstractGeneric
    extends Backend\App\Action
{

}