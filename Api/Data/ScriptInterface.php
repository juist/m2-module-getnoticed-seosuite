<?php

namespace GetNoticed\SeoSuite\Api\Data;

/**
 * Interface ScriptInterface
 *
 * @package GetNoticed\SeoSuite\Api\Data
 */
interface ScriptInterface
{

    /**
     * @return string
     */
    public function getLabel(): string;

    /**
     * @param string $abel
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setLabel(string $abel): ScriptInterface;

    /**
     * @return string
     */
    public function getHtml(): string;

    /**
     * @param string $html
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setHtml(string $html): ScriptInterface;

    /**
     * @return string
     */
    public function getLayoutPosition(): string;

    /**
     * @param string $layoutPosition
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setLayoutPosition(string $layoutPosition): ScriptInterface;

    /**
     * @return string
     */
    public function getOrderPosition(): string;

    /**
     * @param string $orderPosition
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setOrderPosition(string $orderPosition): ScriptInterface;

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime;

    /**
     * @param \DateTime $createdAt
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setCreatedAt(\DateTime $createdAt): ScriptInterface;

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime;

    /**
     * @param \DateTime $updatedAt
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setUpdatedAt(\DateTime $updatedAt): ScriptInterface;

    /**
     * @return \Magento\Store\Api\Data\StoreInterface[]
     */
    public function getStores(): array;

    /**
     * @param array $stores
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setStores(array $stores): ScriptInterface;

    /**
     * @param array $storeIds
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setStoresByIds(array $storeIds): ScriptInterface;

}