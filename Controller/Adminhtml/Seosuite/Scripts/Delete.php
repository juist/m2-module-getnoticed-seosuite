<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use GetNoticed\SeoSuite;
use Magento\Backend;
use Magento\Framework;

/**
 * Class Delete
 *
 * @package GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts
 * @method Framework\App\Request\Http getRequest()
 */
class Delete
    extends AbstractWithEntity
{

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // Variables
        /** @var Framework\Controller\Result\Redirect $response */
        $response = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $response->setPath('*/*');

        // Load existing script, or create a new one
        try {
            $script = $this->getScript();
        } catch (\Exception $e) {
            $script = $this->scriptFactory->create();
        }

        // Try to remove script
        try {
            $this->scriptResource->delete($script);

            $this->messageManager->addSuccessMessage(__('You removed the script.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }

        return $response;
    }

}