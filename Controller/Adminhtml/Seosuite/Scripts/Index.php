<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use Magento\Backend;
use Magento\Framework;

/**
 * Class Index
 *
 * @package GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts
 */
class Index
    extends AbstractGeneric
{

    /**
     * Admin Resource
     */
    const ADMIN_RESOURCE = 'GetNoticed_SeoSuite::scripts';

    /**
     * @return Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /**
         * @var Backend\Model\View\Result\Page $page
         */
        $page = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_PAGE);
        $page->getConfig()->getTitle()->set('Scripts');

        return $page;
    }

}