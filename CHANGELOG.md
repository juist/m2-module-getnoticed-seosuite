# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2017-12-01
### Added
* [PHON-36](http://jira.getnoticed.nl:8080) : Initial setup for SeoSuite module
  * Add SEO title/description to categories
  * Add Script Manager which allows adding/removing of scripts from admin  

### Changed
* [PHON-265](http://jira.getnoticed.nl:8080) : Fixed a bug where the module could not be installed in unison with GetNoticed_Common