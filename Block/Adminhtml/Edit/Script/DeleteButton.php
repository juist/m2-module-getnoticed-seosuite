<?php

namespace GetNoticed\SeoSuite\Block\Adminhtml\Edit\Script;

/**
 * Class DeleteButton
 *
 * @package GetNoticed\SeoSuite\Block\Adminhtml\Edit\Script
 */
class DeleteButton
    extends AbstractButton
{

    /**
     * Delete button
     *
     * @return array
     */
    public function getButtonData()
    {
        if ($this->getEntityId() === null) {
            return [];
        }

        return [
            'id'         => 'delete',
            'label'      => __('Delete'),
            'on_click'   => 'deleteConfirm(\'' . __(
                    'Are you sure you want to do this?'
                ) . '\', \'' . $this->getDeleteUrl() . '\')',
            'class'      => 'delete',
            'sort_order' => 20
        ];
    }

    /**
     * @param array $args
     *
     * @return string
     */
    public function getDeleteUrl(array $args = [])
    {
        $params = array_merge($this->getDefaultUrlParams(), $args);

        return $this->getUrl('*/*/delete', $params);
    }

    /**
     * @return array
     */
    protected function getDefaultUrlParams()
    {
        return ['_current' => true, '_query' => ['isAjax' => null]];
    }

}