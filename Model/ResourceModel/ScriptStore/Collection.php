<?php

namespace GetNoticed\SeoSuite\Model\ResourceModel\ScriptStore;

use GetNoticed\SeoSuite;

/**
 * Class Collection
 *
 * @package GetNoticed\SeoSuite\Model\ResourceModel\ScriptStore
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = self::ID_FIELD_NAME;

    /**
     * Id Field Name
     */
    const ID_FIELD_NAME = 'link_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            SeoSuite\Model\ScriptStore::class,
            SeoSuite\Model\ResourceModel\ScriptStore::class
        );
    }

}