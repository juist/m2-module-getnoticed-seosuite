<?php

namespace GetNoticed\SeoSuite\Setup;

use Magento\Framework;
use Magento\Eav;
use Magento\Catalog;

/**
 * Class UpgradeData
 *
 * @package GetNoticed\SeoSuite\Setup
 */
class UpgradeData
    implements \Magento\Framework\Setup\UpgradeDataInterface
{

    /**
     * @var Eav\Setup\EavSetupFactory
     */
    protected $eavSetupFactory;

    /**
     * @var Eav\Setup\EavSetup
     */
    protected $eavSetup;

    /**
     * @inheritDoc
     */
    public function __construct(
        Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function upgrade(
        Framework\Setup\ModuleDataSetupInterface $setup,
        Framework\Setup\ModuleContextInterface $context
    ) {
        // Initialize EAV setup
        $this->eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

        // Start setup
        $setup->startSetup();

        // Update scripts
        if (version_compare($context->getVersion(), '0.2.0', '<=')) {
            $this->addCatalogCategoryAttributes($setup, $setup->getConnection());
        }

        // Finish setup
        $setup->endSetup();
    }

    /**
     * Create attributes
     *
     * @param Framework\Setup\ModuleDataSetupInterface $setup
     * @param Framework\DB\Adapter\AdapterInterface    $adapter
     */
    protected function addCatalogCategoryAttributes(
        Framework\Setup\ModuleDataSetupInterface $setup,
        Framework\DB\Adapter\AdapterInterface $adapter
    ) {
        $this->eavSetup->addAttribute(
            Catalog\Model\Category::ENTITY,
            'seo_title',
            [
                'type'       => 'text',
                'label'      => 'SEO Title',
                'required'   => false,
                'sort_order' => 100,
                'global'     => Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE
            ]
        );
        $this->eavSetup->addAttribute(
            Catalog\Model\Category::ENTITY,
            'seo_description',
            [
                'type'                     => 'text',
                'label'                    => 'SEO Description',
                'input'                    => 'textarea',
                'required'                 => false,
                'sort_order'               => 110,
                'global'                   => Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                'wysiwyg_enabled'          => true,
                'is_html_allowed_on_front' => true
            ]
        );
    }

}