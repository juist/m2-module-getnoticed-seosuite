<?php

namespace GetNoticed\SeoSuite\Model;

use GetNoticed\SeoSuite;
use GetNoticed\SeoSuite\Api\Data\ScriptStoreInterface;

/**
 * @method \GetNoticed\SeoSuite\Model\ResourceModel\ScriptStore getResource()
 * @method \GetNoticed\SeoSuite\Model\ResourceModel\ScriptStore\Collection getCollection()
 */
class ScriptStore
    extends \Magento\Framework\Model\AbstractModel
    implements \GetNoticed\SeoSuite\Api\Data\ScriptStoreInterface,
               \Magento\Framework\DataObject\IdentityInterface
{

    /**
     * Cache Tag
     */
    const CACHE_TAG = 'getnoticed_seosuite_scriptstore';

    /**
     * @var string
     */
    protected $_cacheTag = 'getnoticed_seosuite_scriptstore';

    /**
     * @var string
     */
    protected $_eventPrefix = 'getnoticed_seosuite_scriptstore';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(SeoSuite\Model\ResourceModel\ScriptStore::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @inheritDoc
     */
    public function setScriptId(int $storeId): ScriptStoreInterface
    {
        return $this->setData('script_id', $storeId);
    }

    /**
     * @inheritDoc
     */
    public function setStoreId(int $storeId): ScriptStoreInterface
    {
        return $this->setData('store_id', $storeId);
    }

}