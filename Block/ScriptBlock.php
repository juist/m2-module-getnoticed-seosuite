<?php

namespace GetNoticed\SeoSuite\Block;

use Magento\Framework;
use Magento\Store;
use GetNoticed\SeoSuite;

/**
 * Class ScriptBlock
 *
 * @package GetNoticed\SeoSuite\Block
 */
class ScriptBlock
    extends Framework\View\Element\Template
{

    /**
     * @var string
     */
    protected $layoutPosition;

    /**
     * @var string
     */
    const DEFAULT_TEMPLATE = 'GetNoticed_SeoSuite::script-block.phtml';

    /**
     * @var SeoSuite\Model\ResourceModel\Script\CollectionFactory
     */
    protected $scriptCollectionFactory;

    /**
     * @var SeoSuite\Model\ResourceModel\Script\Collection
     */
    protected $scriptCollection;

    /**
     * @inheritDoc
     */
    public function __construct(
        SeoSuite\Model\ResourceModel\Script\CollectionFactory $scriptCollectionFactory,
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = []
    ) {
        // DI
        $this->scriptCollectionFactory = $scriptCollectionFactory;

        // Check required parameters
        if (!array_key_exists('layoutPosition', $data) || strlen($data['layoutPosition']) < 1) {
            throw new \BadMethodCallException('Missing required argument "layoutPosition" in $data of ' . self::class);
        }

        $this->layoutPosition = $data['layoutPosition'];

        // Set default template
        $this->setTemplate(self::DEFAULT_TEMPLATE);

        // Call Parent Constructor
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getLayoutPosition(): string
    {
        return $this->layoutPosition;
    }

    /**
     * @return \GetNoticed\SeoSuite\Model\ResourceModel\Script\Collection
     */
    public function getScriptsCollection(): SeoSuite\Model\ResourceModel\Script\Collection
    {
        if ($this->scriptCollection === null) {
            $this->scriptCollection = $this->scriptCollectionFactory->create();
            $this->scriptCollection->addFieldToSelect('html');
            $this->scriptCollection->addFieldToFilter('layout_position', $this->layoutPosition);
            $this->scriptCollection->addStoreFilter($this->_storeManager->getStore());
            $this->scriptCollection->addOrder('order_position');
        }

        return $this->scriptCollection;
    }

}