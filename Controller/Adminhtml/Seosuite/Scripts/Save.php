<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use GetNoticed\SeoSuite;
use Magento\Backend;
use Magento\Framework;

/**
 * Class Save
 *
 * @package GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts
 * @method Framework\App\Request\Http getRequest()
 */
class Save
    extends AbstractWithEntity
{

    /**
     * @var bool
     */
    protected $returnToEdit = false;

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // Variables
        /** @var Framework\Controller\Result\Redirect $response */
        $response = $this->resultFactory->create(Framework\Controller\ResultFactory::TYPE_REDIRECT);
        $data = $this->getRequest()->getPostValue();

        // Load existing script, or create a new one
        try {
            $script = $this->getScript();
        } catch (\Exception $e) {
            $script = $this->scriptFactory->create();
        }

        // Try to save script
        if (is_array($data) && count($data) > 0) {
            try {
                // Script data
                $scriptData = $data['script'];
                $scriptStores = $scriptData['stores'];
                unset($scriptData['stores']);

                // Add data to script
                $script->addData($scriptData);
                $script->setStoresByIds($scriptStores);

                $this->scriptResource->save($script);
                $this->_getSession()->unsScriptFormData();

                $this->messageManager->addSuccessMessage(__('You saved the script.'));
                $this->returnToEdit = (bool)$this->getRequest()->getParam('back', false);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
                $this->_getSession()->setScriptFormData($data);
                $this->returnToEdit = true;
            }
        }

        if ($this->returnToEdit === true) {
            if ($script->getId() !== null) {
                $response->setPath(
                    '*/*/edit',
                    [
                        'script_id' => $script->getId(),
                        '_current'  => true
                    ]
                );
            } else {
                $response->setPath(
                    '*/*/create',
                    [
                        '_current' => true
                    ]
                );
            }
        } else {
            $response->setPath('*/*');
        }

        return $response;
    }

}