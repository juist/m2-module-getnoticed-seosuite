<?php

namespace GetNoticed\SeoSuite\Block\Adminhtml\Edit\Script;

/**
 * Class SaveButton
 *
 * @package GetNoticed\SeoSuite\Block\Adminhtml\Edit\Script
 */
class SaveButton
    extends AbstractButton
{

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label'          => __($this->getEntityId() === null ? 'Create' : 'Save'),
            'class'          => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save'
            ],
            'sort_order'     => 90
        ];
    }

}