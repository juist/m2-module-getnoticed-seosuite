<?php

namespace GetNoticed\SeoSuite\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl;
use GetNoticed\SeoSuite\Model\ResourceModel\Script as ScriptResource;
use GetNoticed\SeoSuite\Model\ResourceModel\Script\Collection as ScriptCollection;

/**
 * Class InstallSchema
 *
 * @package GetNoticed\SeoSuite\Setup
 */
class InstallSchema
    implements \Magento\Framework\Setup\InstallSchemaInterface
{

    /**
     * Create tables
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface   $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     *
     * @return void
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();
        $this->addScriptTable($setup, $setup->getConnection());
        $this->addScriptStoreTable($setup, $setup->getConnection());
        $setup->endSetup();
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface  $setup
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $adapter
     */
    protected function addScriptTable(
        SchemaSetupInterface $setup,
        AdapterInterface $adapter
    ) {
        $adapter->createTable(
            $adapter
                ->newTable(ScriptResource::TABLE_NAME)
                ->addColumn(
                    ScriptCollection::ID_FIELD_NAME,
                    Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'primary'  => true,
                        'unsigned' => true,
                        'identity' => true,
                        'nullable' => false
                    ],
                    'ID'
                )
                ->addColumn(
                    'label',
                    Ddl\Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false
                    ],
                    'Label'
                )
                ->addColumn(
                    'html',
                    Ddl\Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false
                    ],
                    'HTML'
                )
                ->addColumn(
                    'layout_position',
                    Ddl\Table::TYPE_TEXT,
                    30,
                    [
                        'nullable' => false
                    ],
                    'Layout Position'
                )
                ->addColumn(
                    'order_position',
                    Ddl\Table::TYPE_SMALLINT,
                    4,
                    [
                        'nullable' => false
                    ],
                    'Order Position'
                )
                ->addColumn(
                    'created_at',
                    Ddl\Table::TYPE_DATETIME,
                    null,
                    [
                        'nullable' => false
                    ],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Ddl\Table::TYPE_DATETIME,
                    null,
                    [
                        'nullable' => false
                    ],
                    'Updated At'
                )
                ->setComment('Get.Noticed SEO - Script')
        );
    }

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface  $setup
     * @param \Magento\Framework\DB\Adapter\AdapterInterface $adapter
     */
    public function addScriptStoreTable(
        SchemaSetupInterface $setup,
        AdapterInterface $adapter
    ) {
        $adapter->createTable(
            $adapter
                ->newTable(ScriptResource::TABLE_NAME_STORE)
                ->addColumn(
                    'link_id',
                    Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'primary'  => true,
                        'unsigned' => true,
                        'identity' => true,
                        'nullable' => false
                    ],
                    'Link ID'
                )
                ->addColumn(
                    'script_id',
                    Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Script ID'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        ScriptResource::TABLE_NAME_STORE,
                        'script_id',
                        ScriptResource::TABLE_NAME,
                        'script_id'
                    ),
                    'script_id',
                    ScriptResource::TABLE_NAME,
                    'script_id',
                    Ddl\Table::ACTION_CASCADE
                )
                ->addColumn(
                    'store_id',
                    Ddl\Table::TYPE_SMALLINT,
                    5,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Store ID'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        ScriptResource::TABLE_NAME_STORE,
                        'store_id',
                        $setup->getTable('store'),
                        'store_id'
                    ),
                    'store_id',
                    $setup->getTable('store'),
                    'store_id',
                    Ddl\Table::ACTION_CASCADE
                )
                ->setComment('Get.Noticed SEO - Script Store')
        );
    }

}