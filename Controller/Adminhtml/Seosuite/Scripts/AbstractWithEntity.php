<?php

namespace GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts;

use Magento\Backend;
use Magento\Framework;
use GetNoticed\SeoSuite;

/**
 * Class AbstractWithEntity
 *
 * @package GetNoticed\SeoSuite\Controller\Adminhtml\Seosuite\Scripts
 * @method Framework\App\Request\Http getRequest()
 */
abstract class AbstractWithEntity
    extends AbstractGeneric
{

    /**
     * @var SeoSuite\Model\ScriptFactory
     */
    protected $scriptFactory;

    /**
     * @var SeoSuite\Model\ResourceModel\Script
     */
    protected $scriptResource;

    /**
     * @var Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var SeoSuite\Model\Script
     */
    protected $script;

    /**
     * AbstractWithEntity constructor.
     *
     * @param \Magento\Backend\App\Action\Context             $context
     * @param \GetNoticed\SeoSuite\Model\ScriptFactory        $scriptFactory
     * @param \GetNoticed\SeoSuite\Model\ResourceModel\Script $scriptResource
     * @param \Magento\Framework\Registry                     $coreRegistry
     */
    public function __construct(
        Backend\App\Action\Context $context,
        SeoSuite\Model\ScriptFactory $scriptFactory,
        SeoSuite\Model\ResourceModel\Script $scriptResource,
        Framework\Registry $coreRegistry
    ) {
        $this->scriptFactory = $scriptFactory;
        $this->scriptResource = $scriptResource;
        $this->coreRegistry = $coreRegistry;

        parent::__construct($context);
    }

    /**
     * @return \GetNoticed\SeoSuite\Model\Script
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getScript(): SeoSuite\Model\Script
    {
        if ($this->script === null) {
            $this->script = $this->scriptFactory->create();
            $this->scriptResource->load($this->script, $this->getScriptId());

            if ($this->script->getId() === null) {
                throw new Framework\Exception\NoSuchEntityException(__('Script not found.'));
            }

            $this->coreRegistry->register(
                SeoSuite\Block\Adminhtml\Edit\Script\AbstractButton::CURRENT_ENTITY_ID,
                $this->script->getId()
            );
        }

        return $this->script;
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getScriptId()
    {
        if (($scriptData = $this->getRequest()->getPostValue('script'))
            && array_key_exists('script_id', $scriptData)) {
            return (int)$scriptData['script_id'];
        } elseif ($data = $this->getRequest()->getParam('script_id')) {
            return (int)$data;
        }

        throw new Framework\Exception\LocalizedException(__('No script ID given.'));
    }

}