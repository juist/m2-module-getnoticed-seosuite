<?php

namespace GetNoticed\SeoSuite\Model;

use Magento\Framework;
use GetNoticed\SeoSuite;
use GetNoticed\SeoSuite\Api\Data\ScriptInterface;
use GetNoticed\Common\Data\Form\Element as CommonElement;

/**
 * @method \GetNoticed\SeoSuite\Model\ResourceModel\Script getResource()
 * @method \GetNoticed\SeoSuite\Model\ResourceModel\Script\Collection getCollection()
 */
class Script
    extends Framework\Model\AbstractModel
    implements \GetNoticed\SeoSuite\Api\Data\ScriptInterface,
               Framework\DataObject\IdentityInterface
{

    /**
     * Cache tag
     */
    const CACHE_TAG = 'getnoticed_seosuite_script';

    /**
     * @var string
     */
    protected $_cacheTag = 'getnoticed_seosuite_script';

    /**
     * @var string
     */
    protected $_eventPrefix = 'getnoticed_seosuite_script';

    /**
     * @var CommonElement\Select\Options $scriptOptions
     */
    protected $scriptOptions;

    /**
     * @var SeoSuite\Data\ValidationResultsFactory
     */
    protected $validationResultsFactory;

    /**
     * Script constructor.
     *
     * @param \GetNoticed\Common\Data\Form\Element\Select\ValidateOptionsInterface $scriptOptions
     * @param \GetNoticed\SeoSuite\Data\ValidationResultsFactory                   $validationResultsFactory
     * @param \Magento\Framework\Model\Context                                     $context
     * @param \Magento\Framework\Registry                                          $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource|null         $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb|null                   $resourceCollection
     * @param array                                                                $data
     */
    public function __construct(
        CommonElement\Select\ValidateOptionsInterface $scriptOptions,
        SeoSuite\Data\ValidationResultsFactory $validationResultsFactory,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->scriptOptions = $scriptOptions;
        $this->validationResultsFactory = $validationResultsFactory;

        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(SeoSuite\Model\ResourceModel\Script::class);
    }

    /**
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * @return $this|Framework\Model\AbstractModel
     */
    public function beforeSave()
    {
        $validate = $this->validate();

        if ($validate->isValid() !== true) {
            throw new Framework\Exception\LocalizedException(__('Validation failed, please check errors.'));
        }

        return parent::beforeSave();
    }

    /**
     * @return \GetNoticed\SeoSuite\Api\Data\ValidationResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function validate(): SeoSuite\Api\Data\ValidationResultsInterface
    {
        /** @var \GetNoticed\SeoSuite\Api\Data\ValidationResultsInterface $validationResults */
        $validationResults = $this->validationResultsFactory->create();

        try {
            if (strlen($this->getLabel()) < 1) {
                throw new Framework\Exception\LocalizedException(__('Label is required.'));
            }

            if (strlen($this->getHtml()) < 1) {
                throw new Framework\Exception\LocalizedException(__('HTML is required.'));
            }

            if (strlen($this->getLayoutPosition()) < 1) {
                throw new Framework\Exception\LocalizedException(__('Layout Position is required.'));
            }

            if ($this->scriptOptions->isValidOptionByValue($this->getLayoutPosition()) !== true) {
                throw new Framework\Exception\LocalizedException(__('Invalid Layout Position.'));
            }

            if ((int)$this->getOrderPosition() < 1) {
                throw new Framework\Exception\LocalizedException(
                    __('Invalid order position: must be a positive number.')
                );
            }

            $this->_eventManager->dispatch(
                'getnoticed_seosuite_script_validate',
                [
                    'script' => $this
                ]
            );

            $validationResults
                ->setIsValid(true)
                ->setMessages([]);
        } catch (Framework\Exception\LocalizedException $e) {
            $validationResults
                ->setIsValid(false)
                ->setMessages([$e->getMessage()]);
        }

        return $validationResults;
    }

    /**
     * @inheritDoc
     */
    public function getLabel(): string
    {
        return $this->getData('label');
    }

    /**
     * @inheritDoc
     */
    public function setLabel(string $label): ScriptInterface
    {
        return $this->setData('label', $label);
    }

    /**
     * @inheritDoc
     */
    public function getHtml(): string
    {
        return $this->getData('html');
    }

    /**
     * @inheritDoc
     */
    public function setHtml(string $html): ScriptInterface
    {
        return $this->setData('html', $html);
    }

    /**
     * @inheritDoc
     */
    public function getLayoutPosition(): string
    {
        return $this->getData('layout_position');
    }

    /**
     * @inheritDoc
     */
    public function setLayoutPosition(string $layoutPosition): ScriptInterface
    {
        return $this->setData('layout_position', $layoutPosition);
    }

    /**
     * @inheritDoc
     */
    public function getOrderPosition(): string
    {
        return $this->getData('order_position');
    }

    /**
     * @inheritDoc
     */
    public function setOrderPosition(string $orderPosition): ScriptInterface
    {
        return $this->setData('order_position', $orderPosition);
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): \DateTime
    {
        return new \DateTime($this->getData('created_at'));
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(\DateTime $createdAt): ScriptInterface
    {
        return $this->setData('created_at', $createdAt->format('Y-m-d H:i:s'));
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): \DateTime
    {
        return new \DateTime($this->getData('updated_at'));
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt(\DateTime $updatedAt): ScriptInterface
    {
        return $this->setData('updated_at', $updatedAt->format('Y-m-d H:i:s'));
    }

    /**
     * @inheritDoc
     */
    public function getStores(): array
    {
        return $this->getData('stores');
    }

    /**
     * @inheritDoc
     */
    public function setStores(array $stores): ScriptInterface
    {
        return $this->setData('stores', $stores);
    }

    /**
     * @param array $storeIds
     *
     * @return \GetNoticed\SeoSuite\Api\Data\ScriptInterface
     */
    public function setStoresByIds(array $storeIds): ScriptInterface
    {
        $this->getResource()->loadStoresByIds($this, $storeIds);

        return $this;
    }

}